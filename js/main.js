$(document).ready(function () {
    $(".filter-sides ul li.f-tab").on('click', function () {
        var class_name = $(this).data("tab");
        $(".filter-sides ul li").removeClass("active");
        $(this).addClass("active");
        $(".tabs-row .tab-item").removeClass("active");
        $("." + class_name).addClass("active");
    });

    initHomeSwiper();
    initPopularTireSwiper();
    initPopularDiskSwiper();
    initBrandSlider();
    initBlogSwiper();
    initProductSlider();
    initOtherProductSwiper();
    addFilterToggler();
});


function addFilterToggler() {
    $(document).on('click', '.filter-toggler', function () {
        $(".catalog-filters").fadeToggle();
    });
}

function initHomeSwiper() {
    var homeslider = $(".homeslider").slick({
        dots: true,
        arrows: false
    });
}


function initPopularTireSwiper() {
    var swiperTire = $(".swiper-tire").slick({
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 915,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 635,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            }
        ]
    });
    $('.st .mobile-arrows .slick-prev').on('click',function(e){
        e.preventDefault();
        swiperTire.slick('slickPrev');
    });
    $('.st .mobile-arrows .slick-next').on('click',function(e){
        e.preventDefault();
        swiperTire.slick('slickNext');
    });
}


function initOtherProductSwiper() {
    var swiperTire = $(".swiper-other-products").slick({
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1032,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 915,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 635,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            }
        ]
    });

    $('.st .mobile-arrows .slick-prev').on('click',function(e){
        e.preventDefault();
        swiperTire.slick('slickPrev');
    });
    $('.st .mobile-arrows .slick-next').on('click',function(e){
        e.preventDefault();
        swiperTire.slick('slickNext');
    });
}

function initPopularDiskSwiper() {
    var swiperDisk = $(".swiper-disk").slick({
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 915,
                settings: {
                    slidesToShow: 2,
                    arrows:false
                }
            },
            {
                breakpoint: 635,
                settings: {
                    slidesToShow: 1,
                    arrows:false
                }
            }
        ]
    });

    $('.sd .mobile-arrows .slick-prev').on('click',function(e){
        e.preventDefault();
        swiperDisk.slick('slickPrev');
    });
    $('.sd .mobile-arrows .slick-next').on('click',function(e){
        e.preventDefault();
        swiperDisk.slick('slickNext');
    });
}


function initBlogSwiper() {
    var swiperDisk = $(".swiper-blog").slick({
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 915,
                settings: {
                    slidesToShow: 2,
                    arrows:false
                }
            },
            {
                breakpoint: 635,
                settings: {
                    slidesToShow: 1,
                    arrows:false
                }
            }
        ]
    });

    $('.sb .mobile-arrows .slick-prev').on('click',function(e){
        e.preventDefault();
        swiperDisk.slick('slickPrev');
    });
    $('.sb .mobile-arrows .slick-next').on('click',function(e){
        e.preventDefault();
        swiperDisk.slick('slickNext');
    });
}

function initBrandSlider() {
    var brands = $(".brand-list").slick({
        arrows: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        loop: true,
        responsive: [
            {
                breakpoint: 1160,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1050,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 915,
                settings: {
                    slidesToShow: 4,
                    arrows: false
                }
            },
            {
                breakpoint: 695,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            }
        ]
    });
}

function initProductSlider() {
    $('.productslider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        asNavFor: '.productsliderthumb',
    });

    $('.productsliderthumb').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.productslider',
        focusOnSelect: true
    });

    // Remove active class from all thumbnail slides
    $('.productsliderthumb .slick-slide').removeClass('slick-active');

    // Set active class to first thumbnail slides
    $('.productsliderthumb .slick-slide').eq(0).addClass('slick-active');

    // On before slide change match active thumbnail to current slide
    $('.productslider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var mySlideNumber = nextSlide;
        $('.productsliderthumb .slick-slide').removeClass('slick-active');
        $('.productsliderthumb .slick-slide').eq(mySlideNumber).addClass('slick-active');
    });
}